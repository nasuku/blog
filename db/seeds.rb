# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)
post = Post.create([ {title: '1 t', body: '1 b'}] )
c = Comment.create([ {body: '1 c for p 1'} ] )
post[0].comments << c[0]
c = Comment.create([ {body: '2 c for p 1'} ] )
post[0].comments << c[0]
